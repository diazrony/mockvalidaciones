package com.banortecupones.mockvalidaciones.dto;

public class ResponseSolicitaCuponDTO {
    private boolean ok;
    private String mensaje;
    private double porcentaje;
    private String referencia;


    public ResponseSolicitaCuponDTO() {
    }

    public ResponseSolicitaCuponDTO(boolean ok, String mensaje) {
        this.ok = ok;
        this.mensaje = mensaje;
    }

    public ResponseSolicitaCuponDTO(boolean ok, String mensaje, double porcentaje, String referencia) {
        this.ok = ok;
        this.mensaje = mensaje;
        this.porcentaje = porcentaje;
        this.referencia = referencia;
    }

    public ResponseSolicitaCuponDTO(String mensaje, double porcentaje, String referencia) {
        this.mensaje = mensaje;
        this.porcentaje = porcentaje;
        this.referencia = referencia;
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public double getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(double porcentaje) {
        this.porcentaje = porcentaje;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }
}
