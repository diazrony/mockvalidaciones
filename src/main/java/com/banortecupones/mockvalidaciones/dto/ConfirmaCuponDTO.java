package com.banortecupones.mockvalidaciones.dto;

public class ConfirmaCuponDTO {
    private String codigo;
    private String referencia;
    private String canal;

    public ConfirmaCuponDTO(String codigo, String referencia, String canal) {
        this.codigo = codigo;
        this.referencia = referencia;
        this.canal = canal;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getCanal() {
        return canal;
    }

    public void setCanal(String canal) {
        this.canal = canal;
    }
}
