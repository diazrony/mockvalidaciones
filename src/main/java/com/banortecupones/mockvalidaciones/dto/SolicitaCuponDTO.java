package com.banortecupones.mockvalidaciones.dto;

public class SolicitaCuponDTO {
    private String ramo;
    private String producto;
    private String paquete;
    private String codigo;

    public SolicitaCuponDTO(String ramo, String producto, String paquete, String codigo) {
        this.ramo = ramo;
        this.producto = producto;
        this.paquete = paquete;
        this.codigo = codigo;
    }

    public String getRamo() {
        return ramo;
    }

    public void setRamo(String ramo) {
        this.ramo = ramo;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getPaquete() {
        return paquete;
    }

    public void setPaquete(String paquete) {
        this.paquete = paquete;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
}
