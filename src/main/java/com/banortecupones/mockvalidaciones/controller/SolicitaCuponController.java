package com.banortecupones.mockvalidaciones.controller;

import com.banortecupones.mockvalidaciones.dto.ResponseSolicitaCuponDTO;
import com.banortecupones.mockvalidaciones.dto.SolicitaCuponDTO;
import com.banortecupones.mockvalidaciones.service.SolicitaCuponService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/solicitacupon")
public class SolicitaCuponController {

    private SolicitaCuponService solicitaCuponService;

    public SolicitaCuponController(SolicitaCuponService solicitaCuponService) {
        this.solicitaCuponService = solicitaCuponService;
    }

    @PostMapping
    public ResponseEntity<ResponseSolicitaCuponDTO> confirmaCupon(@RequestBody SolicitaCuponDTO solicitaCuponDTO) {
        ResponseSolicitaCuponDTO responseSolicitaCuponDTO = this.solicitaCuponService.validarCupon(solicitaCuponDTO.getCodigo());
        return ResponseEntity.ok(responseSolicitaCuponDTO);
    }
}
