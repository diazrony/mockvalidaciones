package com.banortecupones.mockvalidaciones.controller;

import com.banortecupones.mockvalidaciones.dto.ConfirmaCuponDTO;
import com.banortecupones.mockvalidaciones.dto.ResponseConfirmaCuponDTO;
import com.banortecupones.mockvalidaciones.service.ConfirmaCuponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/confirmacupon")
public class ConfirmaCuponController {
    private ConfirmaCuponService confirmaCuponService;

    public ConfirmaCuponController(ConfirmaCuponService confirmaCuponService) {
        this.confirmaCuponService = confirmaCuponService;
    }
    @PostMapping
    public ResponseConfirmaCuponDTO confirmaCupon(@RequestBody ConfirmaCuponDTO confirmaCuponDTO) {
        ResponseConfirmaCuponDTO responseConfirmaCuponDTO = this.confirmaCuponService.confirmarCupon(confirmaCuponDTO.getCodigo(), confirmaCuponDTO.getReferencia());
        return responseConfirmaCuponDTO;
    }
}
