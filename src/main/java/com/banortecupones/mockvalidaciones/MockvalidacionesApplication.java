package com.banortecupones.mockvalidaciones;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MockvalidacionesApplication {

    public static void main(String[] args) {
        SpringApplication.run(MockvalidacionesApplication.class, args);
    }

}
