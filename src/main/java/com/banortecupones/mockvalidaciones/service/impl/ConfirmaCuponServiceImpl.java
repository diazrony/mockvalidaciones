package com.banortecupones.mockvalidaciones.service.impl;

import com.banortecupones.mockvalidaciones.dto.ResponseConfirmaCuponDTO;
import com.banortecupones.mockvalidaciones.service.ConfirmaCuponService;
import org.springframework.stereotype.Service;

@Service
public class ConfirmaCuponServiceImpl implements ConfirmaCuponService {

    @Override
    public ResponseConfirmaCuponDTO confirmarCupon(String cupon, String referencia) {
        ResponseConfirmaCuponDTO responseConfirmaCuponDTO = new ResponseConfirmaCuponDTO(false, "No hacen match los codigos");
        if ( cupon.equals("32CE7FF5KG") && referencia.equals("101fd812-3994-42fb-8bf1-d540635d5592") ) {
            responseConfirmaCuponDTO.setMensaje("Confirmación exitosa");
            responseConfirmaCuponDTO.setOk(true);
        }
        if ( cupon.equals("ZKBFUEXNKR") && referencia.equals("514da29b-3426-4c16-a2c5-1a43e8d8f1a2") ) {
            responseConfirmaCuponDTO.setMensaje("Código no vigente");
            responseConfirmaCuponDTO.setOk(false);
        }
        if ( cupon.equals("3YD5B6S83C") && referencia.equals("5d46fc57-59dc-486a-89ec-37e57913d484") ) {
            responseConfirmaCuponDTO.setMensaje("Código no vigente");
            responseConfirmaCuponDTO.setOk(false);
        }
        if ( cupon.equals("K4GAWXJ6PP") && referencia.equals("f5703e3b-f373-4f09-bb67-922f1531fec7") ) {
            responseConfirmaCuponDTO.setMensaje("El código ha superado su límite de usos");
            responseConfirmaCuponDTO.setOk(false);
        }
        if ( cupon.equals("W9URHY6R7X") && referencia.equals("00ddbf79-ceea-4a1c-a74d-50268efc310d") ) {
            responseConfirmaCuponDTO.setMensaje("El código ha superado su límite de usos");
            responseConfirmaCuponDTO.setOk(false);
        }
        return responseConfirmaCuponDTO;
    }
}
