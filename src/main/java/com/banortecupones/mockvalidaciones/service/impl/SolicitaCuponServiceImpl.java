package com.banortecupones.mockvalidaciones.service.impl;

import com.banortecupones.mockvalidaciones.dto.ResponseSolicitaCuponDTO;
import com.banortecupones.mockvalidaciones.service.SolicitaCuponService;
import org.springframework.stereotype.Service;

@Service
public class SolicitaCuponServiceImpl implements SolicitaCuponService {

    @Override
    public ResponseSolicitaCuponDTO validarCupon(String codigoCupon) {
        ResponseSolicitaCuponDTO responseSolicitaCuponDTO = new ResponseSolicitaCuponDTO(false,"Codigo no encontrado");
        if (codigoCupon.equals("32CE7FF5KG") || codigoCupon.equals("ZKBFUEXNKR") || codigoCupon.equals("3YD5B6S83C") || codigoCupon.equals("K4GAWXJ6PP") || codigoCupon.equals("W9URHY6R7X") ) {
            responseSolicitaCuponDTO.setMensaje("Código Válido");
            responseSolicitaCuponDTO.setOk(true);
            switch (codigoCupon) {
                case "32CE7FF5KG":
                    responseSolicitaCuponDTO.setPorcentaje(10);
                    responseSolicitaCuponDTO.setReferencia("101fd812-3994-42fb-8bf1-d540635d5592");
                    break;
                case "ZKBFUEXNKR":
                    responseSolicitaCuponDTO.setPorcentaje(20);
                    responseSolicitaCuponDTO.setReferencia("514da29b-3426-4c16-a2c5-1a43e8d8f1a2");
                    break;
                case "3YD5B6S83C":
                    responseSolicitaCuponDTO.setPorcentaje(30);
                    responseSolicitaCuponDTO.setReferencia("5d46fc57-59dc-486a-89ec-37e57913d484");
                    break;
                case "K4GAWXJ6PP":
                    responseSolicitaCuponDTO.setPorcentaje(40);
                    responseSolicitaCuponDTO.setReferencia("f5703e3b-f373-4f09-bb67-922f1531fec7");
                    break;
                case "W9URHY6R7X":
                    responseSolicitaCuponDTO.setPorcentaje(50);
                    responseSolicitaCuponDTO.setReferencia("00ddbf79-ceea-4a1c-a74d-50268efc310d");
                    break;
            }
        }
        if (codigoCupon.equals("PM56UQPERG") || codigoCupon.equals("LX48UCZP2W") || codigoCupon.equals("T2DYVRFKB7") || codigoCupon.equals("YS2AKLQDBM") || codigoCupon.equals("G2D7SK4JQB") ) {
            responseSolicitaCuponDTO.setMensaje("El código no se encuentra activo");
            responseSolicitaCuponDTO.setOk(false);
        }

        if (codigoCupon.equals("BYSBZ3BPRW") || codigoCupon.equals("59U22PEXFT") || codigoCupon.equals("QZSL8PPTJP") || codigoCupon.equals("G4FBSP36GP") || codigoCupon.equals("WVZK5CLHMH") ) {
            responseSolicitaCuponDTO.setMensaje("Código no vigente");
            responseSolicitaCuponDTO.setOk(false);
        }

        if (codigoCupon.equals("XJJK2V32ME") || codigoCupon.equals("YALZMSJF9D") || codigoCupon.equals("5752PS5SEK") || codigoCupon.equals("V5LWC9V4R8") || codigoCupon.equals("39L4T7SPSL") ) {
            responseSolicitaCuponDTO.setMensaje("El código ha superado su límite de usos");
            responseSolicitaCuponDTO.setOk(false);
        }

        if (codigoCupon.equals("F9FZJVLTNZ") || codigoCupon.equals("HDYUHENMYB") || codigoCupon.equals("QYZ5S8HJAA") || codigoCupon.equals("9FGB24PZHW") || codigoCupon.equals("EA9JA2KCEW") ) {
            responseSolicitaCuponDTO.setMensaje("Condiciones no válidas para el uso de este cupón");
            responseSolicitaCuponDTO.setOk(false);
        }
        return responseSolicitaCuponDTO;
    }
}
