package com.banortecupones.mockvalidaciones.service;

import com.banortecupones.mockvalidaciones.dto.ResponseSolicitaCuponDTO;

public interface SolicitaCuponService {
    ResponseSolicitaCuponDTO validarCupon(String codigoCupon);
}
