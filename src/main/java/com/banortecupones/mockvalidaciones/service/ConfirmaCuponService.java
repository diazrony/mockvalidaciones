package com.banortecupones.mockvalidaciones.service;

import com.banortecupones.mockvalidaciones.dto.ResponseConfirmaCuponDTO;
import org.springframework.stereotype.Service;

public interface ConfirmaCuponService {
    ResponseConfirmaCuponDTO confirmarCupon(String codigo, String referencia);
}
